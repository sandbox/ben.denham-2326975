<?php

/**
 * @file
 * Contains \Drupal\dochurd\Form\SettingsForm.
 */

namespace Drupal\dochurd\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure DocHurd Integration settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'dochurd.settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('dochurd.settings');

    $form['dochurd_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('DocHurd API key'),
      '#default_value' => $config->get('dochurd_api_key'),
      '#description' => t('The DocHurd API key for your site.'),
      '#maxlength' => 36,
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('dochurd.settings')
      ->set('dochurd_api_key', $form_state->getValue('dochurd_api_key'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
